<div class="container">
    <div class="row justify-content-center h-100">
        <div class="col-lg-5">
            <div class="mt-5">

                <div class="text-center">
                    <h1 class="text-dark-50 text-center mt-0 font-weight-bold">{$title}</h1>
                    <p class="text-muted mb-4">{$error_code}</p>
                </div>

                <div class="card-footer2">
                    <div class="col-12 text-center text-nowrap mt-2">
                        <a href="{$site_path}/" class="btn btn-primary btn-lg"><span class="fas fa-home"></span> {_Take Me Home} </a>
                        <!--BeginSezButtonSupport-->
                        <a href="mailto:{$email_support}" class="btn btn-default btn-lg"><span class="fas fa-envelope"></span> {_Contact Support} </a>
                        <!--EndSezButtonSupport-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
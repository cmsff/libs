<?php
/**
 * Library for WebApplication based on VGallery Framework
 * Copyright (C) 2004-2021 Alessandro Stucchi <wolfgan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  @package VGallery
 *  @subpackage libs
 *  @author Alessandro Stucchi <wolfgan@gmail.com>
 *  @copyright Copyright (c) 2004, Alessandro Stucchi
 *  @license http://opensource.org/licenses/lgpl-3.0.html
 *  @link https://bitbucket.org/cmsff/libs
 */
namespace ff\libs\gui\adapters;

use ff\libs\cache\Buffer;
use ff\libs\Constant;
use ff\libs\Debug;
use ff\libs\gui\Controller;
use ff\libs\gui\Resource;
use ff\libs\gui\View;
use ff\libs\Hook;
use ff\libs\international\Locale;
use ff\libs\international\Translator;
use ff\libs\Kernel;
use ff\libs\storage\FilemanagerFs;
use ff\libs\Exception;
use stdClass;

/**
 * Class ViewHtml
 * @package ff\libs\gui\adapters
 */
class ViewHtml implements ViewAdapter
{
    protected const REGEXP                      = '/\{([\w\:\=\-\|\.\s\?\!\\\'\"\,\$\/]+)\}/U';
    protected const REGEXP_STRIP                = '/\{\$(.+)\}/U';

    protected const APPLET                      = '/\{\[(.+)\]\}/U';

    private const ERROR_LANG_NOT_VALID          = "lang not valid";
    private const HOOK_ON_FETCH_CONTENT         = 'View::onFetchContent';
    private const TPL_NORMALIZE                 = ['../', '.tpl'];


    public $root_element						= "main";

    public $BeginTag							= "Begin";
    public $EndTag								= "End";

    public $debug_msg							= false;
    public $display_unparsed_sect				= false;

    private $DBlocks 							= [];
    private $DVars 								= [];
    private $DBlockVars 					    = [];
    private $ParsedBlocks 						= [];

    private $cache                              = [];
    private $widget                             = null;
    private $lang				                = null;

    /**
     * @var bool|string[strip|strong_strip|minify]
     */
    public $minify								= false;

    public function __construct(string $widget = null, array &$cache = null)
    {
        $this->widget                           = $widget;
        $this->cache                            =& $cache;
    }

    /**
     * @param string $template_disk_path
     * @return ViewAdapter
     * @throws Exception
     */
    public function fetch(string $template_disk_path) : ViewAdapter
    {
        $this->loadFile($template_disk_path);

        return $this;
    }

    /**
     * @param string $content
     * @param string|null $root_element
     * @return ViewAdapter
     * @throws Exception
     */
    public function fetchContent(string $content, string $root_element = null) : ViewAdapter
    {
        if ($root_element !== null) {
            $this->root_element = $root_element;
        }

        if (!$content) {
            throw new Exception("template empty", 500);
        }

        $this->DBlocks[$this->root_element] = $this->getDVars($content);
        $nName = $this->nextDBlockName($this->root_element);
        while ($nName != "") {
            $this->setBlock($this->root_element, $nName);
            $nName = $this->nextDBlockName($this->root_element);
        }

        Hook::handle(self::HOOK_ON_FETCH_CONTENT, $this);

        return $this;
    }

    /**
     * @param string $name
     * @return bool
     */
    public function isset(string $name) : bool
    {
        return isset($this->DBlocks[$name]);
    }

    /**
     * @param string $template_path
     * @throws Exception
     */
    private function loadFile(string $template_path) : void
    {
        $tpl_name = Translator::infoLangCode($this->lang) . "-" . str_replace(
            [
                Constant::DISK_PATH . "/",
                "_",
                "/"

            ],
            [
                "",
                "-",
                "_"
            ],
            $template_path
        );

        Debug::stopWatch("tpl/" . $tpl_name);

        $cache = Buffer::cache("tpl");

        $res = $cache->get($tpl_name);
        if (!$res) {
            $this->cache = [
                $template_path  => filemtime($template_path)
            ];
            if ($cache_file = Translator::infoCacheFile($this->lang)) {
                $this->cache[$cache_file] = filemtime($cache_file);
            }

            $this->DBlocks[$this->root_element] = $this->include($template_path);

            $nName = $this->nextDBlockName($this->root_element);
            while ($nName != "") {
                $this->setBlock($this->root_element, $nName);
                $this->blockVars($nName);
                $nName = $this->nextDBlockName($this->root_element);
            }

            $cache->set($tpl_name, array(
                "DBlocks"       => $this->DBlocks,
                "DVars"         => $this->DVars,
                "DBlockVars"    => $this->DBlockVars,
                "root_element"  => $this->root_element
            ), $this->cache);
        } else {
            $this->DBlocks      = $res["DBlocks"];
            $this->DVars        = $res["DVars"];
            $this->DBlockVars   = $res["DBlockVars"];
            $this->root_element = $res["root_element"];
        }

        Hook::handle(self::HOOK_ON_FETCH_CONTENT, $this);

        Debug::stopWatch("tpl/" . $tpl_name);
    }

    /**
     * @param string $content
     * @return string
     * @throws Exception
     */
    private function getDVars(string $content) : string
    {
        $content                                            = preg_replace(self::REGEXP_STRIP, '{$1}', $content);

        $matches                                            = null;
        $rc                                                 = preg_match_all(static::REGEXP, $content, $matches);
        if ($rc && $matches) {
            $DVars                                          = array_fill_keys($matches[1], []);

            $theme_disk_path                                = Kernel::$Environment::getThemeDiskPath();
            $views                                          = Resource::views($this->widget);
            $translation                                    = new stdClass();
            foreach ($DVars as $nName => $count) {
                if (substr($nName, 0, 1) == "_") {
                    $translation->key[]                     = "{" . $nName . "}";
                    $translation->value[]                   = Translator::getWordByCode(substr($nName, 1), $this->lang);
                    unset($DVars[$nName]);
                } elseif (substr($nName, 0, 7) == "include" && substr_count($nName, '"') == 2) {
                    $template_file                          =  explode('"', $nName)[1];
                    $template                               = $views[str_replace(self::TPL_NORMALIZE, '', $template_file)] ?? str_replace('$theme_path', $theme_disk_path, $template_file);

                    $view                                   = new self($this->widget, $this->cache);
                    $include                                = $view->include($template);
                    $this->DVars                            = array_replace($this->DVars, $view->DVars);
                    $this->DBlockVars                       = array_replace($this->DBlockVars, $view->DBlockVars);

                    $this->cache[$template]                 = filemtime($template);

                    $content                                = str_replace("{" . $nName . "}", $include, $content);
                } elseif (strpos($nName, "::") !== false) {
                    $component                              = explode("::", $nName);

                    $DVars[$component[0]][$nName]           = $component[1];
                    unset($DVars[$nName]);
                }
            }
            if (isset($translation->key)) {
                $content = str_replace($translation->key, $translation->value, $content);
            }

            $this->DVars                                    = array_replace($this->DVars, $DVars);
        }

        return $content;
    }

    /**
     * @param string $sTemplateName
     * @return string
     */
    private function nextDBlockName(string $sTemplateName) : string
    {
        $sTemplate = $this->DBlocks[$sTemplateName];
        $BTag = strpos($sTemplate, "<!--" . $this->BeginTag);
        if ($BTag === false) {
            return "";
        } else {
            $ETag = strpos($sTemplate, "-->", $BTag);
            $sName = substr($sTemplate, $BTag + 9, $ETag - ($BTag + 9));
            if (strpos($sTemplate, "<!--" . $this->EndTag . $sName . "-->") > 0) {
                return $sName;
            } else {
                return "";
            }
        }
    }

    /**
     * @param string $sTplName
     * @param string $sBlockName
     */
    private function setBlock(string $sTplName, string $sBlockName) : void
    {
        if (!isset($this->DBlocks[$sBlockName])) {
            $this->DBlocks[$sBlockName] = $this->getBlock($this->DBlocks[$sTplName], $sBlockName);
        }
        $this->DBlocks[$sTplName] = $this->replaceBlock($this->DBlocks[$sTplName], $sBlockName);

        $nName = $this->nextDBlockName($sBlockName);

        while ($nName != "") {
            $this->setBlock($sBlockName, $nName);
            $nName = $this->nextDBlockName($sBlockName);
        }
    }

    /**
     * @param string $sTemplate
     * @param string $sName
     * @return string
     */
    private function getBlock(string $sTemplate, string $sName) : string
    {
        $alpha = strlen($sName) + 12;

        $BBlock = strpos($sTemplate, "<!--" . $this->BeginTag . $sName . "-->");
        $EBlock = strpos($sTemplate, "<!--" . $this->EndTag . $sName . "-->");

        if ($BBlock === false || $EBlock === false) {
            return "";
        } else {
            return substr($sTemplate, $BBlock + $alpha, $EBlock - $BBlock - $alpha);
        }
    }

    /**
     * @param string $sTemplate
     * @param string $sName
     * @return string
     */
    private function replaceBlock(string $sTemplate, string $sName) : string
    {
        $BBlock = strpos($sTemplate, "<!--" . $this->BeginTag . $sName . "-->");
        $EBlock = strpos($sTemplate, "<!--" . $this->EndTag . $sName . "-->");

        if ($BBlock === false || $EBlock === false) {
            return $sTemplate;
        } else {
            return substr($sTemplate, 0, $BBlock) . "{" . $sName . "}" . substr($sTemplate, $EBlock + strlen("<!--End" . $sName . "-->"));
        }
    }

    /**
     * @param string $sName
     * @return string
     */
    public function getVar(string $sName) : string
    {
        return $this->DBlocks[$sName];
    }

    /**
     * @param string $sName
     * @return bool
     */
    public function issetVar(string $sName) : bool
    {
        return isset($this->DVars[$sName]) || isset($this->DBlocks[$sName]);
    }

    /**
     * @param string $sName
     * @return bool
     */
    public function issetBlock(string $sName) : bool
    {
        return (bool)($this->ParsedBlocks[$sName]);
    }

    /**
     * @param string $sectionName
     * @param bool $repeat
     * @param bool $appendBefore
     * @return bool
     * @throws Exception
     */
    public function parse(string $sectionName, bool $repeat = false, bool $appendBefore = false) : bool
    {
        if (isset($this->DBlocks[$sectionName])) {
            if ($repeat && isset($this->ParsedBlocks[$sectionName])) {
                if ($appendBefore) {
                    $this->ParsedBlocks[$sectionName] = $this->proceedTpl($sectionName) . $this->ParsedBlocks[$sectionName];
                } else {
                    $this->ParsedBlocks[$sectionName] .= $this->proceedTpl($sectionName);
                }
            } else {
                $this->ParsedBlocks[$sectionName] = $this->proceedTpl($sectionName);
            }
            return true;
        } elseif ($this->debug_msg) {
            echo "<br><strong>Block with name <u><span style=\\";
        }

        return false;
    }

    /**
     * @param array|string $tpl_var
     * @param mixed|null $value
     * @return $this
     */
    public function assign($tpl_var, $value = null) : ViewAdapter
    {
        if (is_array($tpl_var)) {
            //@todo da togliere e inserire nella view principale
            $this->ParsedBlocks             = array_replace($this->ParsedBlocks, $tpl_var);
        } elseif(is_object($tpl_var)) {
            $this->ParsedBlocks             = array_replace($this->ParsedBlocks, (array) $tpl_var);
        } else {
            $this->ParsedBlocks[$tpl_var]   = $value;
        }

        return $this;
    }

    /**
     * @return string
     * @throws Exception
     */
    public function display() : string
    {
        $this->setAssignDefault();

        /**
         * Components
         */
        foreach (array_intersect_key(Resource::components(), $this->DVars)  as $key => $component) {
            /**
             * @var Controller $controller
             */
            $controller = (new $component());
            if (!empty($this->DVars[$key])) {
                foreach ($this->DVars[$key] as $DVar => $method) {
                    $this->assign($DVar, $controller->html($method));
                }
            } else {
                $this->assign($key, $controller->html());
            }
        }

        /**
         * AutoTpl
         */
        foreach (array_diff_key($this->DBlockVars, $this->ParsedBlocks) as $key => $DBlockVars) {
            $lowerKey = strtolower($key);
            if (isset($this->ParsedBlocks[$lowerKey])) {
                if (is_array($this->ParsedBlocks[$lowerKey])) {
                    $this->ParsedBlocks[$key] = $this->proceedAutoTpl($key, $this->ParsedBlocks[$lowerKey]);
                    if (isset($this->DBlockVars[$key . "Index"])) {
                        $this->ParsedBlocks[$key . "Index"] = $this->proceedAutoTpl($key . "Index", $this->ParsedBlocks[$lowerKey]);
                    }
                } elseif (!empty($this->ParsedBlocks[$lowerKey])) {
                    $this->ParsedBlocks[$key] = $this->proceedTpl($key);
                }
            }
        }
        $this->parse($this->root_element);
        return $this->getBlockContent($this->root_element);
    }

    /**
     * @param string $sTplName
     * @param array $blocks
     * @return string|null
     */
    private function proceedAutoTpl(string $sTplName, array $blocks) : ?string
    {
        $res = null;
        $vars = $this->DBlockVars[$sTplName];
        $sTpl = $this->DBlocks[$sTplName];
        foreach ($blocks as $i => $block) {
            $search_for = [];
            $replace_with = [];
            foreach ($vars as $value) {
                $search_for[] = "{" . $value . "}";
                if (isset($block[$value])) {
                    $replace_with[] = $block[$value];
                } elseif (isset($this->ParsedBlocks[$value]) && !isset($this->ParsedBlocks[$value][0]) && !is_array($this->ParsedBlocks[$value][0])) {
                    $replace_with[] = $this->ParsedBlocks[$value];
                } elseif ($i === 0 && strpos($value, ":first=") !== false) {
                    $replace_with[] = explode("=", $value, 2)[1];
                } elseif ($i === count($blocks) -1 && strpos($value, ":last=") !== false) {
                    $replace_with[] = explode("=", $value, 2)[1];
                } elseif (strpos($value, ":" . $i . "=") !== false) {
                    $replace_with[] = explode("=", $value, 2)[1];
                } elseif (strpos($value, ":index") !== false) {
                    $replace_with[] = $i;
                } else {
                    $replace_with[] = "";
                }
            }
            $res .= str_replace($search_for, $replace_with, $sTpl);
        }

        return $res;
    }

    /**
     * @param string $template_path
     * @return string
     * @throws Exception
     */
    private function include(string $template_path) : string
    {
        if (!($content = FilemanagerFs::fileGetContents($template_path))) {
            throw new Exception("Unable to find the template: " . $template_path, 500);
        }
        return $this->getDVars($content);
    }

    /**
     * @param string $block_name
     * @param string|null $minify
     * @return string|null
     * @throws Exception
     */
    private function getBlockContent(string $block_name, string $minify = null) : ?string
    {
        $minify = ($minify === null ? $this->minify : $minify);

        if ($minify === false) {
            return (isset($this->ParsedBlocks[$block_name])
                ? $this->entitiesReplace($this->ParsedBlocks[$block_name])
                : null
            );
        } elseif ($minify === "strip") {
            return $this->entitiesReplace(preg_replace("/\n\s*/", "\n", $this->ParsedBlocks[$block_name], -1, $count));
        } elseif ($minify === "strong_strip") {
            return $this->entitiesReplace(preg_replace(
                array(
                    '/>[^\S ]+/s',  // strip whitespaces after tags, except space
                    '/[^\S ]+</s',  // strip whitespaces before tags, except space
                    '/(\s)+/s'       // shorten multiple whitespace sequences
                ),
                array(
                    '>',
                    '<',
                    '\\1'
                ),
                $this->ParsedBlocks[$block_name],
                -1,
                $count
            ));
        } else {
            throw new Exception("minify method not implemented", 501);
        }
    }

    /**
     * @param string $sTplName
     * @return array|null
     */
    private function blockVars(string $sTplName) : ?array
    {
        if (isset($this->DBlockVars[$sTplName])) {
            return $this->DBlockVars[$sTplName];
        }

        $sTpl = $this->DBlocks[$sTplName];

        $matches = array();
        $rc = preg_match_all(static::REGEXP, $sTpl, $matches);
        if ($rc) {
            $vars = $matches[1];

            $this->DBlockVars[$sTplName] = $vars;

            return $vars;
        } else {
            return null;
        }
    }

    /**
     * @param string $sTplName
     * @return string
     * @throws Exception
     */
    private function proceedTpl(string $sTplName) : string
    {
        $vars = $this->blockVars($sTplName);
        $sTpl = $this->DBlocks[$sTplName];

        if ($vars) {
            $search_for = array();
            $replace_with = array();

            reset($vars);
            foreach ($vars as $value) {
                $search_for[] = "{" . $value . "}";
                if (isset($this->ParsedBlocks[$value])) {
                    if (is_object($this->ParsedBlocks[$value])) {
                        if ($this->ParsedBlocks[$value] instanceof View || $this->ParsedBlocks[$value] instanceof Controller) {
                            $replace_with[] = $this->ParsedBlocks[$value]->html();
                        } else {
                            throw new Exception("bad value into template", 500);
                        }
                    } elseif (is_array($this->ParsedBlocks[$value])) {
                        $replace_with[] = null;
                    } else {
                        $replace_with[] = $this->ParsedBlocks[$value];
                    }
                } elseif (isset($this->DBlocks[$value]) && $this->display_unparsed_sect) {
                    $replace_with[] = $this->DBlocks[$value];
                } else {
                    $replace_with[] = "";
                }
            }
            $sTpl = str_replace($search_for, $replace_with, $sTpl);
        }
        return $sTpl;
    }

    /**
     * @param string $text
     * @return string
     */
    private function entitiesReplace(string $text) : string
    {
        return str_replace(array("{\\","\\}"), array("{","}"), $text);
    }


    /**
     *
     */
    private function setAssignDefault() : void
    {
        $this->assign("site_path", Kernel::$Environment::SITE_PATH);
    }

    /**
     * @param string|null $lang_code
     * @return ViewHtml
     * @throws Exception
     */
    public function setLang(string $lang_code = null) : ViewAdapter
    {
        if ($lang_code && !Locale::isAcceptedLanguage($lang_code)) {
            throw new Exception(self::ERROR_LANG_NOT_VALID, 400);
        }
        $this->lang                        = $lang_code;

        return $this;
    }
}
